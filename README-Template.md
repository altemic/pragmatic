# Checkout Component 3.0

This project is a Rest Webservice, which can scan products, add them to checkout and generate receipt.

## Installing

All dependencies are handled by maven, so there is enough to type "mvn package" in project directory,
to receive jar (with embedded tomcat webserver).
Source code is ready to import it into IDE - I've used JetBrains Intellij.

In order to package application use command: "mvn package" inside project directory.
You may want to change server port, you can do it by overriding defaults in /src/main/resources/application.properties file.
By default WebService is accessible at: http://localhost:8080/

#Usage

	Endpoints:
	http://localhost:8080/checkout
		GET - returns Checkout or Receipt
		POST - with PathVariable "op" and its value set to "pay" [POST http://localhost:8080/checkout?op=pay]
		DELETE - clears checkout

	http://localhost:8080/checkout/entries
		GET - returns all entries on checkout
		DELETE /id - removes checkout with checkoutEntryId
		POST - adds new checkoutEntry
			[Sample below]
				{
					"itemName" : "A",
					"quantity" : 100
				}

	http://localhost:8080/items
		GET - returns list of all items accessible

## Running the tests

In order to run tests use command: "mvn test" inside project directory.

## Running

After packaging the app will have .jar extensions,
Tomcat is embedded into WebService.
To run webservice its enough to use java -jar filename.jar command.

## Built With

* [Spring](https://spring.io) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [JUnit](https://rometools.github.io/rome/) - Used for unit testing
* [Cucumber] (https://cucumber.io/docs/reference/jvm#java) - Used for acceptance testing

#Code notes
There is no database used - repository is just a list of 4 entries.
In real example with lots of items it would be necessary to use one.
Also no login service is implemented, the service uses Spring SessionScoped bean - CheckoutModule which stores
current state of checkout.
Checkout and receipt use the same class, with just different status after payment.
Checkout always serves list of its entries, maybe it would be wiser to return only link to access this collection,
but after reading description of the task, I thought scanning function would be good this way.
After calculating price checkout entries related to the same item are joined into one entry.
