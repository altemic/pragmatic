# Checkout Component 3.0

This project is a Rest Webservice, which can scan products, add them to checkout and generate receipt.

### Installing

All dependencies are handled by maven, so there is enough to type "mvn package" in project directory,
to receive jar (with embedded tomcat webserver).
Source code is ready to import it into IDE - I've used JetBrains Intellij.

In order to package application use command: "mvn package" inside project directory.

## Running the tests

In order to run tests use command: "mvn test" inside project directory.

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Spring](https://spring.io) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [JUnit](https://rometools.github.io/rome/) - Used for unit testing
* [Cucumber] (https://cucumber.io/docs/reference/jvm#java) - Used for acceptance testing

## Authors
Michał Altenberg