package pl.altemic.pragmaticcoders.checkout;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.altemic.pragmaticcoders.checkout.modules.CheckoutModule;
import pl.altemic.pragmaticcoders.checkout.repository.ItemsRepository;
import pl.altemic.pragmaticcoders.checkout.models.CheckoutEntry;
import pl.altemic.pragmaticcoders.checkout.models.Item;
import pl.altemic.pragmaticcoders.checkout.models.Receipt;
import pl.altemic.pragmaticcoders.checkout.tools.PriceCalculator;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CheckoutApplicationUnitTests {

	@Autowired
	private CheckoutModule checkoutModule;

	@Autowired
	private ItemsRepository itemsRepository;

	@Autowired
	private PriceCalculator priceCalculator;

	private CheckoutEntry addCheckoutEntry(String itemName, double quantity){
		CheckoutEntry checkoutEntry = new CheckoutEntry(itemName, 20);
		return checkoutModule.addEntry(checkoutEntry);
	}

	@Test
	public void whenAddedOneCheckoutEntryThenReceiptContainsOneCheckoutEntry(){
		addCheckoutEntry("A", 20);
		checkoutModule.registerPayment();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertEquals(1, receipt.getCheckoutEntries().size());
	}

	@Test
	public void whenAddedThreeCheckoutEntryThenReceiptContainsThreeCheckoutEntries(){
		addCheckoutEntry("A", 5);
		addCheckoutEntry("B", 10);
		addCheckoutEntry("C", 15);
		checkoutModule.registerPayment();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertEquals(3, receipt.getCheckoutEntries().size());
	}

	@Test
	public void whenAddedFourCheckoutEntriesToReceiptAndRemovedOneThenOnlyTwoAreLeft(){
		addCheckoutEntry("A", 5);
		CheckoutEntry checkoutEntry = addCheckoutEntry("B", 10);
		addCheckoutEntry("C", 15);
		checkoutModule.removeEntry(checkoutEntry.getCheckoutEntryId());
		checkoutModule.registerPayment();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertEquals(2, receipt.getCheckoutEntries().size());
	}

	@Test
	public void whenPaymentRequestedThenReceiptIsPaid(){
		addCheckoutEntry("A", 5);
		addCheckoutEntry("B", 10);
		checkoutModule.registerPayment();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertTrue(receipt.isPaid());
	}

	@Test
	public void whenClearedReceiptThenCheckoutIsEmpty(){
		addCheckoutEntry("A", 5);
		addCheckoutEntry("B", 10);
		checkoutModule.clearCheckout();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertTrue(receipt.getCheckoutEntries().isEmpty());
	}

	@Test
	public void whenClearedReceiptThenPriceIsZero(){
		checkoutModule.clearCheckout();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertTrue(receipt.getValue() == 0);
	}

	@Test
	public void whenClearedReceiptThenItIsUnpaid(){
		checkoutModule.clearCheckout();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertFalse(receipt.isPaid());
	}

	@Test
	public void whenUnpaidThenStatusIsCheckout(){
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertFalse(receipt.isPaid());
		Assert.assertEquals("Checkout", receipt.getStatus());
	}

	@Test
	public void whenPaidThenStatusIsReceipt(){
		addCheckoutEntry("A", 5);
		addCheckoutEntry("B", 10);
		checkoutModule.registerPayment();
		Receipt receipt = checkoutModule.getCheckout();
		Assert.assertTrue(receipt.isPaid());
		Assert.assertEquals("Receipt", receipt.getStatus());
	}

	@Test
	public void whenTryingToPayAndCheckoutIsEmptyThenException(){

	}

	@Test
	public void whenSpecialQuantityThenSpecialPrice(){
		for(Item item : itemsRepository.getAll()) {
			CheckoutEntry checkoutEntryWithSpecialQuantity = new CheckoutEntry(item.getName(), item.getSpecialQuantity());
			double specialPrice = priceCalculator.calculate(Arrays.asList(checkoutEntryWithSpecialQuantity));
			CheckoutEntry checkoutEntryWithRegularQuantity = new CheckoutEntry(item.getName(), item.getSpecialQuantity() - 1);
			double regularPrice = priceCalculator.calculate(Arrays.asList(checkoutEntryWithRegularQuantity));
			Assert.assertTrue(specialPrice < regularPrice);
		}
	}

	@Test
	public void whenRegularQuantityThenRegularPrice(){
		for(Item item : itemsRepository.getAll()) {
			CheckoutEntry checkoutEntryWithRegularQuantity = new CheckoutEntry(item.getName(), item.getSpecialQuantity() - 1);
			double regularPrice = priceCalculator.calculate(Arrays.asList(checkoutEntryWithRegularQuantity));
			CheckoutEntry checkoutEntryWithSpecialQuantity = new CheckoutEntry(item.getName(), item.getSpecialQuantity());
			double specialPrice = priceCalculator.calculate(Arrays.asList(checkoutEntryWithSpecialQuantity));
			Assert.assertTrue(specialPrice < regularPrice);
		}
	}
}
