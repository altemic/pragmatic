package pl.altemic.pragmaticcoders.checkout;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:Feature", glue = "pl.altemic.pragmaticcoders.checkout")
public class CucumberTest {
}
