package pl.altemic.pragmaticcoders.checkout;


import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;
import pl.altemic.pragmaticcoders.checkout.models.CheckoutEntry;
import pl.altemic.pragmaticcoders.checkout.models.Receipt;

import java.util.HashMap;
import java.util.Map;

@ContextConfiguration(classes = CheckoutApplication.class, loader = SpringBootContextLoader.class)
@WebIntegrationTest(randomPort = true)
public class CheckoutApplicationFromZeroToReceipt {

    @Value("${local.server.port}")
    int port;

    HttpHeaders httpHeaders;

    private String getCheckoutUri(){
        String checkoutUri = "http://localhost:" + port + "/checkout";
        return checkoutUri;
    }

    private String getEntriesUri(){
        String entriesUri = getCheckoutUri() + "/entries";
        return entriesUri;
    }

    private <T> ResponseEntity<T> sendPost(String uri, Object obj, Class<T> cls, HttpHeaders httpHeaders){
        RestTemplate restTemplate = new RestTemplate();
        if(httpHeaders != null) {
            HttpEntity entity = prepareHttpEntityWithSessionCookie(obj, httpHeaders);
            ResponseEntity<T> responseEntity = restTemplate.postForEntity(uri, entity, cls, new HashMap<>());
            return responseEntity;
        } else {
            ResponseEntity<T> responseEntity  = restTemplate.postForEntity(uri, obj, cls, new HashMap<>());
            return responseEntity;
        }
    }

    private HttpEntity prepareHttpEntityWithSessionCookie(Object obj, HttpHeaders httpHeaders){
        HttpHeaders _httpHeaders = rewriteSessionCookie(httpHeaders);
        HttpEntity entity = new HttpEntity(obj, _httpHeaders);
        return entity;
    }

    private HttpHeaders rewriteSessionCookie(HttpHeaders httpHeaders){
        Map<String, String> headers = httpHeaders.toSingleValueMap();
        String sessionCookie = headers.get("Set-Cookie");
        HttpHeaders _httpHeaders = new HttpHeaders();
        _httpHeaders.add("Cookie", sessionCookie);
        return _httpHeaders;
    }

    @When("^launched then checkout is empty and its status is Checkout$")
    public void launched_then_checkout_is_empty_and_its_status_is_Checkout() throws Throwable {

        String checkoutUri = getCheckoutUri();

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity entity = new HttpEntity(new HttpHeaders());

        ResponseEntity<Receipt> receiptResponseEntity
                = restTemplate.exchange(checkoutUri, HttpMethod.GET, entity, Receipt.class, new HashMap<>());

        Receipt receipt = receiptResponseEntity.getBody();
        Assert.assertEquals("Checkout", receipt.getStatus());
        Assert.assertEquals(0, receipt.getCheckoutEntries().size());
    }


    @Then("^added four entries and checkout has four entries and filled price$")
    public void added_four_entries_and_checkout_has_four_entries_and_filled_price() throws Throwable {

        String checkoutUri = getCheckoutUri();
        String entriesUri = getEntriesUri();

        ResponseEntity<CheckoutEntry> responseWithSessionId
                = sendPost(entriesUri, new CheckoutEntry("A", 15), CheckoutEntry.class, null);
        httpHeaders = responseWithSessionId.getHeaders();
        sendPost(entriesUri, new CheckoutEntry("B", 250), CheckoutEntry.class, httpHeaders);
        sendPost(entriesUri, new CheckoutEntry("C", 150), CheckoutEntry.class, httpHeaders);
        sendPost(entriesUri, new CheckoutEntry("D", 30), CheckoutEntry.class, httpHeaders);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = rewriteSessionCookie(httpHeaders);
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<Receipt> receiptResponseEntity
                = restTemplate.exchange(checkoutUri, HttpMethod.GET, entity, Receipt.class, new HashMap<>());
        Receipt receipt = receiptResponseEntity.getBody();

        Assert.assertEquals(4, receipt.getCheckoutEntries().size());
        Assert.assertTrue(receipt.getValue() > 0);
    }
    @Then("^removed one entry and checkout has three entries without the deleted$")
    public void removed_second_entry_and_checkout_has_three_entries_without_deleted_one() throws Throwable {

        String checkoutUri = getCheckoutUri();
        String entriesUri = getEntriesUri();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = rewriteSessionCookie(httpHeaders);
        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<Receipt> receiptResponseEntityBeforeDeletion
                = restTemplate.exchange(checkoutUri, HttpMethod.GET, entity, Receipt.class, new HashMap<>());

        Receipt receiptBeforeDeletion = receiptResponseEntityBeforeDeletion.getBody();

        int loseNumber = (int) Math.floor(Math.random() * receiptBeforeDeletion.getCheckoutEntries().size() + 1);

        Assert.assertTrue(
                receiptBeforeDeletion.getCheckoutEntries().stream().anyMatch(e -> e.getCheckoutEntryId() == loseNumber));

        entriesUri += "/" + loseNumber;
        restTemplate.exchange(entriesUri, HttpMethod.DELETE, entity, String.class);

        ResponseEntity<Receipt> receiptResponseEntityAfterDeletion
                = restTemplate.exchange(checkoutUri, HttpMethod.GET, entity, Receipt.class, new HashMap<>());
        Receipt receiptAfterDeletion = receiptResponseEntityAfterDeletion.getBody();

        Assert.assertFalse(
                receiptResponseEntityAfterDeletion.getBody().getCheckoutEntries().stream().anyMatch(e -> e.getCheckoutEntryId() == loseNumber));

        Assert.assertTrue(
                receiptBeforeDeletion.getCheckoutEntries().size() - receiptAfterDeletion.getCheckoutEntries().size() == 1);
    }

    @Then("^payment was done and receipt was generated with status Receipt$")
    public void payment_was_done_and_receipt_was_generated_with_status_Receipt() throws Throwable {

        String checkoutUri = getCheckoutUri() + "/?op=pay";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = rewriteSessionCookie(httpHeaders);
        HttpEntity entity = new HttpEntity(headers);
        restTemplate.exchange(checkoutUri, HttpMethod.POST, entity, String.class);

        ResponseEntity<Receipt> receiptResponseEntity
                = restTemplate.exchange(checkoutUri, HttpMethod.GET, entity, Receipt.class, new HashMap<>());
        Receipt receipt = receiptResponseEntity.getBody();

        Assert.assertEquals(true, receipt.isPaid());
        Assert.assertEquals("Receipt", receipt.getStatus());
    }

    @Then("^tried to add another entry and error was thrown$")
    public void tried_to_add_another_entry_and_error_was_thrown() throws Throwable {

        String entriesUri = getEntriesUri();

        Exception exThrown = null;
        try {
            ResponseEntity<CheckoutEntry> responseWithSessionId
                    = sendPost(entriesUri, new CheckoutEntry("A", 15), CheckoutEntry.class, httpHeaders);
        } catch (Exception e){
            exThrown = e;
        }
        if(exThrown == null){
            throw new Exception("No exception was thrown while delete of entry on paid receipt is forbidden");
        }
    }

    @Then("^cleared checkout and no entry was present$")
    public void cleared_checkout_and_no_entry_was_present() throws Throwable {

        String checkoutUri = getCheckoutUri();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = rewriteSessionCookie(httpHeaders);
        HttpEntity entity = new HttpEntity(headers);
        restTemplate.exchange(checkoutUri, HttpMethod.DELETE, entity, String.class);

        ResponseEntity<Receipt> receiptResponseEntity
                = restTemplate.exchange(checkoutUri, HttpMethod.GET, entity, Receipt.class, new HashMap<>());
        Receipt receipt = receiptResponseEntity.getBody();

        Assert.assertEquals(0, receipt.getCheckoutEntries().size());
    }
}
