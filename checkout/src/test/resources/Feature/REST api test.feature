Feature: Testing a REST API

    Scenario: From placing order through payment to generating receipt
        When launched then checkout is empty and its status is Checkout
        Then added four entries and checkout has four entries and filled price
        Then removed one entry and checkout has three entries without the deleted
        Then payment was done and receipt was generated with status Receipt
        Then tried to add another entry and error was thrown
        Then cleared checkout and no entry was present