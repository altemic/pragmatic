package pl.altemic.pragmaticcoders.checkout.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.altemic.pragmaticcoders.checkout.repository.ItemsRepository;
import pl.altemic.pragmaticcoders.checkout.models.Item;
import java.util.List;

@RestController
@RequestMapping("/items")
public class ItemsController {

    @Autowired
    private ItemsRepository itemsRepository;

    @GetMapping
    public List<Item> getAll(){
        return itemsRepository.getAll();
    }

    @GetMapping("/{name}")
    public Item findByName(@PathVariable String name){
        return itemsRepository.findByName(name);
    }
}
