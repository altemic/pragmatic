package pl.altemic.pragmaticcoders.checkout.repository;

import org.springframework.stereotype.Component;
import pl.altemic.pragmaticcoders.checkout.models.Item;
import pl.altemic.pragmaticcoders.checkout.modules.CheckoutModule;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ItemsRepository implements Repository<Item> {
    private List<Item> allItems;

    @PostConstruct
    private void init(){
        allItems = new LinkedList<>();
        allItems.add(new Item("A", 40, "3", 70));
        allItems.add(new Item("B", 10, "2", 15));
        allItems.add(new Item("C", 30, "4", 60));
        allItems.add(new Item("D", 25, "2", 40));
    }

    @Override
    public List<Item> getAll() {
        return allItems;
    }

    @Override
    public Item findByName(String name){
        List<Item> itemsMatching = allItems.stream()
                .filter(i -> i.getName().equals(name)).collect(Collectors.toList());
        if(!itemsMatching.isEmpty()){
            return itemsMatching.get(0);
        }
        return null;
    }
}
