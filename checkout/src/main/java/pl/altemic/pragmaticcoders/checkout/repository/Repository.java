package pl.altemic.pragmaticcoders.checkout.repository;

import java.util.List;

public interface Repository<T> {
    List<T> getAll();
    T findByName(String name);
}
