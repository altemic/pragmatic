package pl.altemic.pragmaticcoders.checkout.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.*;
import pl.altemic.pragmaticcoders.checkout.modules.CheckoutModule;
import pl.altemic.pragmaticcoders.checkout.models.CheckoutEntry;
import pl.altemic.pragmaticcoders.checkout.models.Receipt;

import javax.el.MethodNotFoundException;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/checkout")
public class CheckoutController {

    @Autowired
    CheckoutModule checkoutModule;

    @RequestMapping(method = RequestMethod.POST)
    public void makePayment(@RequestParam(value = "op") String op) {
        if (op.equals("pay")) {
            checkoutModule.registerPayment();
        } else {
            throw new MethodNotFoundException("This operation is not supported");
        }
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void clearCheckout(){
        checkoutModule.clearCheckout();
    }

    @RequestMapping(method = RequestMethod.GET)
    public Receipt get(){
        Receipt receipt = checkoutModule.getCheckout();
        receipt.removeLinks();
        receipt.add(linkTo(CheckoutController.class).withSelfRel());
        List<CheckoutEntry> methodLinkBuilder =
                methodOn(CheckoutEntriesController.class).getAllEntries();
        Link checkoutEntriesLink = linkTo(methodLinkBuilder).withRel("checkoutEntries");
        receipt.add(checkoutEntriesLink);
        return receipt;
    }
}
