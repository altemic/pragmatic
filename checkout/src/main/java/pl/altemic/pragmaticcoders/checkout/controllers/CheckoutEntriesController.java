package pl.altemic.pragmaticcoders.checkout.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.*;
import pl.altemic.pragmaticcoders.checkout.models.CheckoutEntry;
import pl.altemic.pragmaticcoders.checkout.modules.CheckoutModule;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by m.altenberg on 28.09.2017.
 */
@RestController
@RequestMapping("/checkout/entries")
public class CheckoutEntriesController {

    @Autowired
    CheckoutModule checkoutModule;

    @RequestMapping(method = RequestMethod.POST)
    public CheckoutEntry addEntry(@RequestBody CheckoutEntry checkoutEntry){
        checkoutEntry = checkoutModule.addEntry(checkoutEntry);
        checkoutEntry.removeLinks();
        Link linkToEntry =
                linkTo(CheckoutEntriesController.class).slash(checkoutEntry.getCheckoutEntryId()).withSelfRel();
        checkoutEntry.add(linkToEntry);
        return checkoutEntry;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<CheckoutEntry> getAllEntries(){
        List<CheckoutEntry> checkoutEntries = checkoutModule.getCheckout().getCheckoutEntries();
        for(CheckoutEntry checkoutEntry : checkoutEntries) {
            checkoutEntry.removeLinks();
            Link linkToEntry =
                    linkTo(CheckoutEntriesController.class).slash(checkoutEntry.getCheckoutEntryId()).withSelfRel();
            checkoutEntry.add(linkToEntry);
        }
        return checkoutEntries;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void removeEntry(@PathVariable long id){
        checkoutModule.removeEntry(id);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public CheckoutEntry getEntry(@PathVariable long id){
        CheckoutEntry checkoutEntry = checkoutModule.getEntry(id);
        checkoutEntry.removeLinks();
        Link linkToEntry =
                linkTo(CheckoutEntriesController.class).slash(checkoutEntry.getCheckoutEntryId()).withSelfRel();
        checkoutEntry.add(linkToEntry);
        return checkoutEntry;
    }
}
