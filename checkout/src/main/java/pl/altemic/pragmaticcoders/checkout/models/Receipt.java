package pl.altemic.pragmaticcoders.checkout.models;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class Receipt extends ResourceSupport {
    private List<CheckoutEntry> checkoutEntries;
    private double value;
    private boolean paid;
    private String status = "Checkout";

    public List<CheckoutEntry> getCheckoutEntries() {
        return checkoutEntries;
    }

    public void setCheckoutEntries(List<CheckoutEntry> basketEntries) { this.checkoutEntries = basketEntries; }

    public double getValue() {
        return value;
    }

    public void setValue(double price) {
        this.value = price;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean _paid) {
        if(!paid) {
            this.paid = _paid;
        }
        if(paid){
            status = "Receipt";
        }
    }

    public String getStatus() {
        return status;
    }
}
