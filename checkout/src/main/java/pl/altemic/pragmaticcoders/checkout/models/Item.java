package pl.altemic.pragmaticcoders.checkout.models;

public class Item {

    private String name;
    private double price;
    private String unit;
    private int specialQuantity;

    public Item(String _name, double _price, String _unit, int _specialPriceAmount){
        name = _name;
        price = _price;
        unit = _unit;
        specialQuantity = _specialPriceAmount;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public int getSpecialQuantity() {
        return specialQuantity;
    }
}
