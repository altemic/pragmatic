package pl.altemic.pragmaticcoders.checkout.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.altemic.pragmaticcoders.checkout.repository.ItemsRepository;
import pl.altemic.pragmaticcoders.checkout.repository.Repository;
import pl.altemic.pragmaticcoders.checkout.models.CheckoutEntry;
import pl.altemic.pragmaticcoders.checkout.models.Item;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Component
public class PriceCalculatorImpl implements PriceCalculator{

    private Repository<Item> itemsRepository;
    private final double COST_REDUCTION_FACTOR = 0.7;

    @Autowired
    public PriceCalculatorImpl(Repository<Item> _itemsRepository){
        itemsRepository = _itemsRepository;
    }

    public double calculate(Collection<CheckoutEntry> checkoutEntries){
        double value = 0;

        Map<String, List<CheckoutEntry>> groupedByUnitCheckoutEntries
                = checkoutEntries.stream().collect(Collectors.groupingBy(ce -> itemsRepository.findByName(ce.getItemName()).getUnit()));

        for(Map.Entry<String, List<CheckoutEntry>> groupedByUnitCheckoutEntry : groupedByUnitCheckoutEntries.entrySet()){

            List<CheckoutEntry> sameUnitCheckoutEntries = groupedByUnitCheckoutEntries.get(groupedByUnitCheckoutEntry.getKey());

            sameUnitCheckoutEntries.forEach(e -> getValue(e));

            CheckoutEntry checkoutEntryWithLowerValue
                    = sameUnitCheckoutEntries.stream().min(Comparator.comparingDouble(CheckoutEntry::getValue)).get();

            if(sameUnitCheckoutEntries.size() > 1) {
                checkoutEntryWithLowerValue.setPrice(checkoutEntryWithLowerValue.getPrice() * COST_REDUCTION_FACTOR);
                checkoutEntryWithLowerValue.setValue(checkoutEntryWithLowerValue.getValue() * COST_REDUCTION_FACTOR);
            }
        }

        for(CheckoutEntry checkoutEntry : checkoutEntries){
            value += checkoutEntry.getValue();
        }

        return value;
    }

    private double getValue(CheckoutEntry checkoutEntry){
        double quantity = checkoutEntry.getQuantity();
        Item item = itemsRepository.findByName(checkoutEntry.getItemName());
        double specialQuantity = item.getSpecialQuantity();
        if(quantity >= specialQuantity){
            double specialPrice = item.getPrice() * COST_REDUCTION_FACTOR;
            checkoutEntry.setPrice(specialPrice);
            checkoutEntry.setValue(specialPrice * quantity);
        } else {
            double price = item.getPrice();
            checkoutEntry.setPrice(price);
            checkoutEntry.setValue(price * quantity);
        }
        return checkoutEntry.getValue();
    }
}
