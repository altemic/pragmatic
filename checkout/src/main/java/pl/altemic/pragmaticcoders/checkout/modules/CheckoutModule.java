package pl.altemic.pragmaticcoders.checkout.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.annotation.SessionScope;
import pl.altemic.pragmaticcoders.checkout.models.CheckoutEntry;
import pl.altemic.pragmaticcoders.checkout.models.Item;
import pl.altemic.pragmaticcoders.checkout.models.Receipt;
import pl.altemic.pragmaticcoders.checkout.repository.Repository;
import pl.altemic.pragmaticcoders.checkout.tools.PriceCalculator;

import java.util.*;
import java.util.stream.Collectors;

@Component
@SessionScope
public class CheckoutModule {

    private PriceCalculator priceCalculator;
    private Receipt receipt;
    private Repository<Item> itemRepository;

    @Autowired
    public CheckoutModule(PriceCalculator _priceCalculator, Repository<Item> _itemRepository){
        priceCalculator = _priceCalculator;
        itemRepository = _itemRepository;
        generateNewReceipt();
    }

    private void generateNewReceipt(){
        receipt = new Receipt();
        receipt.setCheckoutEntries(new ArrayList<>());
    }

    public CheckoutEntry addEntry(CheckoutEntry checkoutEntry){
        if(!receipt.isPaid()) {

            if(itemRepository.findByName(checkoutEntry.getItemName()) == null){
                throw new ResourceNotFoundException(String.format("Item with name: %s was not found", checkoutEntry.getItemName()));
            }

            List<CheckoutEntry> entriesWithTheSameItemName
                    = receipt.getCheckoutEntries().stream()
                    .filter(x -> checkoutEntry.getItemName().equals(x.getItemName())).collect(Collectors.toList());

            double value =
                    priceCalculator.calculate(Arrays.asList(checkoutEntry));
            checkoutEntry.setValue(value);

            if(entriesWithTheSameItemName.size() > 0){
                CheckoutEntry entryWithTheSameName = entriesWithTheSameItemName.get(0);
                entryWithTheSameName.setQuantity(entryWithTheSameName.getQuantity() + checkoutEntry.getQuantity());
                entryWithTheSameName.setValue(entryWithTheSameName.getValue() + checkoutEntry.getValue());
                return entryWithTheSameName;
            }

            if (receipt.getCheckoutEntries().isEmpty()) {
                checkoutEntry.setCheckoutEntryId(1);
            } else {
                CheckoutEntry entryWithHighestId
                        = receipt.getCheckoutEntries().stream().max(Comparator.comparingLong(CheckoutEntry::getCheckoutEntryId)).get();
                long maxId = entryWithHighestId.getCheckoutEntryId();
                checkoutEntry.setCheckoutEntryId(maxId + 1);
            }

            receipt.getCheckoutEntries().add(checkoutEntry);
            return checkoutEntry;

        } else {
            throw new PaidReceiptException("Cannot add new entry to already paid receipt!");
        }
    }

    public void removeEntry(long id){
        if(!receipt.isPaid()) {
            List<CheckoutEntry> checkoutEntry = receipt.getCheckoutEntries().stream().filter(be -> be.getCheckoutEntryId() == id).collect(Collectors.toList());
            if (checkoutEntry.isEmpty()) {
                throw new ResourceNotFoundException(String.format("Entry with id: %d was not found", id));
            }
            receipt.getCheckoutEntries().remove(checkoutEntry.get(0));
        } else {
            throw new PaidReceiptException("Cannot remove entry from already paid receipt");
        }
    }

    public CheckoutEntry getEntry(long id){
        List<CheckoutEntry> checkoutEntries = this.receipt.getCheckoutEntries().stream().filter(e -> e.getCheckoutEntryId() == id).collect(Collectors.toList());
        if(checkoutEntries.isEmpty()){
            throw new ResourceNotFoundException(String.format("Entry with id: %d was not found", id));
        }
        return checkoutEntries.get(0);
    }

    public Receipt getCheckout(){
        double price = priceCalculator.calculate(receipt.getCheckoutEntries());
        receipt.setValue(price);
        return receipt;
    }

    public void clearCheckout(){
        generateNewReceipt();
    }

    public void registerPayment(){
        if(receipt.getCheckoutEntries().isEmpty()){
            throw new PaidReceiptException("You cannot pay when checkout is empty");
        }
        if(!receipt.isPaid()) {
            receipt.setPaid(true);
        } else {
            throw new PaidReceiptException("The receipt is already paid");
        }
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException{
        public ResourceNotFoundException(String message){
            super(message);
        }
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    public class PaidReceiptException extends RuntimeException{
        public PaidReceiptException(String message){
            super(message);
        }
    }

    @ResponseStatus(HttpStatus.PAYMENT_REQUIRED)
    public class PaymentRequiredException extends RuntimeException{
        public PaymentRequiredException(String message) { super(message); }
    }
}
