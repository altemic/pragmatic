package pl.altemic.pragmaticcoders.checkout.tools;

import pl.altemic.pragmaticcoders.checkout.models.CheckoutEntry;

import java.util.Collection;

public interface PriceCalculator {
    double calculate(Collection<CheckoutEntry> checkoutEntries);
}
