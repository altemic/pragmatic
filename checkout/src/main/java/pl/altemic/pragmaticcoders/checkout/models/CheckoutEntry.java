package pl.altemic.pragmaticcoders.checkout.models;

import org.springframework.hateoas.ResourceSupport;

public class CheckoutEntry extends ResourceSupport {
    long checkoutEntryId;
    public String itemName;
    public double quantity;
    public double price;
    public double value;

    public CheckoutEntry(){}

    public CheckoutEntry(String _itemName, double _quantity){
        itemName = _itemName;
        quantity = _quantity;
    }

    public long getCheckoutEntryId() {
        return checkoutEntryId;
    }

    public void setCheckoutEntryId(long checkoutEntryId) {
        this.checkoutEntryId = checkoutEntryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
